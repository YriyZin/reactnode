const express = require('express');
const routes = require('./routes/index');
const moviesRoute = require('./routes/movies');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer  = require('multer');

require('dotenv').config();

let app = express();
const PORT = process.env.PORT || 5000;

const dbURL = process.env.MONGO_DB_URL;

mongoose.connect(dbURL, { useNewUrlParser: true }, function(err){
    if(err){
        console.log('Error connecting to: '+ dbURL)
    }
    else{
        console.log('Connected to: '+ dbURL)
    }
})

app.use(multer({dest:"uploads"}).single("filedata"));

app.use(cors());
app.options('*', cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', routes);
app.use('/movies', moviesRoute);

app.listen(PORT, function () {
    console.log(`Listening on port ${PORT}`);
});