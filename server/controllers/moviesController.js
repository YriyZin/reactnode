const Movies = require('../models/Movies')

module.exports = {
    create: (params, callback) => {
        Movies.create(params, (err, result) => {
            if(err){
                callback(err, null);
                return;
            }
            callback(null, result);
        });
    },

    find: (params, callback) => {
        Movies.find(params, '_id title releaseYear format stars', (err, results) => {
            if(err){
                callback(err, null);
                return;
            }
            callback(null, results);
        });
    },

    remove: (params, callback) => {
        Movies.remove(params, {justOne: true}, (err, results) => {
            if(err) {
                callback(err, null);
                return;
            }
            callback(null, results);
        })
    },

    findById: (id, callback) => {
        Movies.findById(id, (err, results) => {
            if(err){
                callback(err, null);
                return;
            }
            callback(null, results);
        });
    }
}