const express = require('express');
const router = express.Router();
const fs = require("fs");

const moviesController = require('../controllers/moviesController')

router.post('/create', (req, res) => {
    moviesController.create(req.body,(err, result) => {
        if(err) {
            res.json({
                success: 0,
                error: err
            })
            return;
        }

        res.json({
            success: 1,
            data: result
        });
    });
});

router.post('/create/availability', (req, res) => {
    moviesController.find(req.body,(err, result) => {
        if(result.length != 0) {
            res.json({
                success: 0,
                error: "Фильм уже существует"
            });
            return;
        }
        res.json({
            success: 1
        });
    });
});

router.post('/import', (req, res) => {
    let filedata = req.file;
    let fileContent = fs.readFileSync(`./uploads/${filedata.filename}`, "utf8");
    let array = fileContent.split(/\n/);
    let parseArray = [];
    let object = {};
    if(filedata.mimetype !== "text/plain") {
        res.json({
            success: 0
        })
        return;
    }
    for(let i = 0; i < array.length; i++) {
        let temporary = array[i].split(':');
        if (temporary[1]) {
            for(let k = 0; k < temporary.length; k++) {
                let key = (temporary[0].charAt(0).toLowerCase() + temporary[0].slice(1)).replace(' ', '');
                let value = temporary[1].trim();
                if(key === "stars") {
                    value = value.split(', ');
                }
                object[key] = value;
            }
            if (array[i + 1] === "\r") {
                parseArray.push(object);
                object = {};
            }
        }
    }
    res.json({
        success: 1,
        data: parseArray
    });
});

router.delete('/remove', (req, res) => {
    moviesController.remove(req.body, (err, result) => {
        if(err){
            res.json({
                success: 0,
                error: err
            })
            return;
        }

        res.json({
            success: 1,
            data: result
        });
    });
});

router.get('/', (req, res) => {
    moviesController.find(req.query, (err, results) => {
        if(err){
            res.json({
                success: 0,
                error: err
            });
            return;
        }
        res.json({
            success: 1,
            data: results
        });
    });
});

router.post('/', (req, res) => {
    moviesController.find(req.body, (err, results) => {
        if(err){
            res.json({
                success: 0,
                error: err
            });
            return;
        }
        res.json({
            success: 1,
            data: results
        });
    });
});

router.post('/stars', (req, res) => {
    moviesController.find(req.body, (err, results) => {
        if(err){
            res.json({
                success: 0,
                error: err
            });
            return;
        }
        res.json({
            success: 1,
            data: results
        });
    });
});

router.get('/:id', (req, res) => {
    const id = req.params.id;

    moviesController.findById(id, (err, result) => {
    
        if(err){
            res.status(500).json({
                success: 0,
                data: result
            });
            return;
        }

        res.status(200).json({
            success: 1,
            data: result
        });
    });
});

module.exports = router