const mongoose = require('mongoose');  

const MoviesSchema = new mongoose.Schema({
    title: String,
    releaseYear: String,
    format: String,
    stars: Array,
    status: {
        type: Number,
        default: 1
    },
    created: {
        type: Date,
        required: true,
        default: new Date()
    }      
});

mongoose.model('Movies', MoviesSchema);

module.exports = mongoose.model('Movies');