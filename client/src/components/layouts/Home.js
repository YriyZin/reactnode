import React, {Component} from 'react';
import Movies from '../containers/Movies';

class Home extends Component {
    render() {
        return ( 
        <div>
            <Movies/>
        </div>
        )
    }
}

export default Home;