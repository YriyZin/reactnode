import React, { Component } from 'react';
import SearchMovies from '../containers/SearchMovies';
 
class Layout extends Component {
    render() {
        return (
            <div>
                <div>
                    <SearchMovies/>
                </div>
                <div>
                    { this.props.children }
                </div>
            </div>
        );
    }
}

export default Layout;