import React, { Component} from 'react';
import PropTypes from 'prop-types';
import { removeMoviesItem } from '../../actions/actions';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { Box, Button } from '@material-ui/core';

class MoviesItemDetail extends Component {

    async removeMoviesItem() {
        if(window.confirm("Вы действительно хотите удалить фильм?")) {
            await this.props.dispatch(removeMoviesItem({title: this.props.data.title}));
            this.props.history.push("/");
        }
    }

    render(){
        return (
            <Box display="flex" flexDirection="column" alignItems="center">
                <h2>Название: {this.props.data.title}</h2>
                <p>Дата выхода: {this.props.data.releaseYear}</p>
                <p>Формат: {this.props.data.format}</p>
                <Box display="flex" mb={3}>
                    Актеры: 
                    {
                        this.props.data.stars.map( (star, i) => {
                            return ( <div key={i}>{star} ,</div> );
                        })
                    }
                </Box>
                <Button variant="contained" color="primary" onClick={this.removeMoviesItem.bind(this)}>Удалить фильм</Button>
            </Box>
        )
    }
}

MoviesItemDetail.propTypes = {
    data: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        releaseYear: PropTypes.string.isRequired,
        format: PropTypes.string.isRequired,
        stars: PropTypes.array.isRequired
    })
};

const mapStateToProps = state => {
    return {}
}

export default withRouter(connect(mapStateToProps)(MoviesItemDetail))