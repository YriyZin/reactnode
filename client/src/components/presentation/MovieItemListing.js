import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Box } from '@material-ui/core';

class MoviesItemListing extends Component {
    render() {
        return (
            <Box textAlign="center" mb={2}>
                <Link to={`/movies/${this.props.data._id}`} className={"app-link"}><strong>{this.props.data.title}</strong></Link>
            </Box>
        )
    }
}

MoviesItemListing.propTypes = {
    data: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired
    })
};

export default MoviesItemListing