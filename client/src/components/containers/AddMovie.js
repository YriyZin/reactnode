import React, {Component} from 'react';
import { connect } from 'react-redux';
import { submitAddMovies, availabilityCreateMovie } from '../../actions/actions';
import { withRouter } from "react-router-dom";
import { Button, Box } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Container from '@material-ui/core/Container';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import MomentUtils from '@date-io/moment';
import moment from 'moment';

class MoviesSubmit extends Component {

    constructor() {
        super();
        this.state = {
            submission: {
                title: null,
                releaseYear: moment().format('YYYY-MM-DD'),
                format: "",
                stars: ""
            },
            minDate: moment("1851-01-01").format('YYYY-MM-DD')
        };
    }

    titleChange(event) {
        let fields = Object.assign({}, this.state.submission);
        fields["title"] = event.target.value;
        this.setState({
            submission: fields
        });
    }

    formatChange(event) {
        let fields = Object.assign({}, this.state.submission);
        fields["format"] = event.target.value;
        this.setState({
            submission: fields
        });
    }

    dateChange(date) {
        if(!date) {
            return;
        }
        let fields = Object.assign({}, this.state.submission);
        fields["releaseYear"] = moment(date._d).format('YYYY-MM-DD');
        this.setState({
            submission: fields
        });
    }

    starsChange(event) {
        let fields = Object.assign({}, this.state.submission);
        fields["stars"] = event.target.value.replace(/[^a-zA-Zа-яА-Я, ]/g, '').split(', ');
        this.setState({
            submission: fields
        });
    }

    async submitSubmission() {
        const res = await this.props.dispatch(availabilityCreateMovie(this.state.submission));
        if(res.success) {
            await this.props.dispatch(submitAddMovies(this.state.submission));
            alert("Фильм удачно создан");
            this.props.history.push("/");
        }
        else {
            alert("Данный фильм уже добавлен");
        }
    }

    get formIsFull() {
        return !this.state.submission.title || !this.state.submission.releaseYear || !this.state.submission.format || !this.state.submission.stars
        || (this.state.submission.releaseYear > moment().format('YYYY-MM-DD') || this.state.submission.releaseYear < moment(this.state.minDate).format('YYYY-MM-DD'));
    }

    render() {
        return (
            <Container maxWidth="sm">
                <Box display="flex" flexDirection="column">
                    <FormControl margin={"normal"}>
                        <Input id="title" placeholder={"Название"} type="text" onChange={this.titleChange.bind(this)} />
                    </FormControl>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <KeyboardDatePicker
                            clearable
                            format="YYYY-MM-DD"
                            margin="normal"
                            id="date-picker-inline"
                            label="Date picker inline"
                            value={this.state.submission.releaseYear || new Date()}
                            onChange={this.dateChange.bind(this)}
                            minDate={this.state.minDate}
                            disableFuture={true}
                        />
                    </MuiPickersUtilsProvider>
                    <FormControl margin={"normal"}>
                        <InputLabel id="label-for-format">Формат</InputLabel>
                        <Select
                            labelId="label-for-format"
                            id="format"
                            value={this.state.submission.format}
                            onChange={this.formatChange.bind(this)}
                        >
                            <MenuItem value={"VHS"}>VHS</MenuItem>
                            <MenuItem value={"DVD"}>DVD</MenuItem>
                            <MenuItem value={"Blu-Ray"}>Blu-Ray</MenuItem>
                        </Select>
                    </FormControl>
                    <div>
                        (Имена актеров перечислят через ", ")
                    </div>
                    <textarea value={this.state.submission.stars} onChange={this.starsChange.bind(this)} id="stars" type="text" placeholder="Актеры фильма"></textarea>
                    <Box textAlign="center" mt={2}>
                        <Button disabled={this.formIsFull} variant="contained" color="primary" onClick={this.submitSubmission.bind(this)}>Добавить фильм</Button>
                    </Box>
                </Box>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {}
}

export default withRouter(connect(mapStateToProps)(MoviesSubmit));