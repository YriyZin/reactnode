import React, {Component} from 'react';
import MovieItemListing from '../presentation/MovieItemListing';
import { connect } from 'react-redux';
import { fetchMovies } from '../../actions/actions';
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';

class Movies extends Component {
    state = {
        movies: []
    }

    async componentDidMount() {
        await this.props.dispatch(fetchMovies());
        this.setState({
            movies: this.props.movies
        });
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.movies !== prevProps.movies) {
            this.setState({
                movies: this.props.movies
            });
        }
    }

    sortMovies() {
        const sortedMovies = [].concat(this.state.movies)
        .sort((a, b) => {
            let nameA = a.title.toLowerCase(), nameB = b.title.toLowerCase();
            if (nameA < nameB)
                return -1
            if (nameA > nameB)
                return 1
            return 0
        })
        .map((item) => {return item});
        this.setState({
            movies: sortedMovies
        });
    }

    render() {

        const movieItems = this.state.movies.map( (movie, i) => {
            return ( <div key={i}><MovieItemListing data = {movie} /></div> );
        });

        return (
            <Box display="flex" flexDirection="column" alignItems="center">
                <h2>Список фильмов</h2>
                <Box>
                    {
                        (this.state.movies.length > 0) ?
                            <div>
                                {
                                    movieItems
                                }
                            </div>
                        :
                            <div>Sorry we have no movies</div>
                    }
                </Box>
                <div>
                    <Button variant="contained" onClick={this.sortMovies.bind(this)} color="primary">
                        Сортировка по алфавиту
                    </Button>
                </div>
            </Box>
        )
    }
}

const mapStateToProps = state => {
    return {
        movies: state.movies.movies
    }
}

export default connect(mapStateToProps)(Movies)