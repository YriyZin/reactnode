import React, {Component} from 'react';
import { connect } from 'react-redux';
import { searchMoviesByName, searchByNameOfActor, importMoviesFromFile, fetchMovies, availabilityCreateMovie, submitAddMovies } from '../../actions/actions';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

class SearchMovies extends Component {
    state = {
        movieName: "",
        actorName: "",
        movies: [],
        file: null,
        isImport: false
    }

    nameMovieOnChange(event) {
        this.setState({
            movieName: event.target.value
        });
    }

    nameActorOnChange(event) {
        this.setState({
            actorName: event.target.value
        });
    }

    async searchByNameMovie() {
        await this.props.dispatch(searchMoviesByName({ title: { $eq: this.state.movieName } }));
        this.setState({
            movies: this.props.movies
        });
    }

    async searchByNameOfActor() {
        await this.props.dispatch(searchByNameOfActor({ stars: { $in: [ this.state.actorName ] } }));
        this.setState({
            movies: this.props.movies
        });
    }

    onFileLoad(event) {
        this.setState({
            file: event.target.files[0]
        });
    }

    async resetSort() {
        this.setState({
            movieName: "",
            actorName: ""
        });
        await this.props.dispatch(fetchMovies());
    }

    async importMovies() {
        this.setState({
            isImport: true
        });
        let formData = new FormData();
        formData.append('filedata', this.state.file);
        const response = await this.props.dispatch(importMoviesFromFile(formData));
        if(!response.success) {
            alert("Неверный тип файла");
            this.setState({
                isImport: false
            });
            return;
        }
        let isAllSuccess = true;
        for(let i = 0; i < response.data.length; i++) {
            let res = await this.props.dispatch(availabilityCreateMovie(response.data[i]));
            if (res.success) {
                await this.props.dispatch(submitAddMovies(response.data[i]));
            } else {
                isAllSuccess = false;
            }
        }
        if(!isAllSuccess) {
            alert("Импорт прошел удачно однако не все фильмы удалось загрузить по причине того что они уже есть в базе");
        } else {
            alert("Все фильмы удачно загружены");
        }
        await this.props.dispatch(fetchMovies());
        this.setState({
            isImport: false
        });
    }

    render() {
        return (
            <div>
                <Box display="flex" justifyContent="center" mt={2} mb={2}>
                    <Box mr={3}>
                        <Link to={'/'} className={"app-link"}>Домашняя</Link>
                    </Box>
                    <div>
                        <Link to={'/submit'} className={"app-link"}>Добавить фильм</Link>
                    </div>
                </Box>
                <Box display="flex" flexDirection="column">
                    <Box display="flex" justifyContent="center" flexWrap="wrap">
                        <Box display="flex" alignItems="center" mr={3}>
                            <FormControl>
                                <InputLabel htmlFor="component-simple">Поиск по названию</InputLabel>
                                <Input id="component-simple" value={this.state.movieName} onChange={this.nameMovieOnChange.bind(this)} />
                            </FormControl>
                            <Button variant="contained" onClick={this.searchByNameMovie.bind(this)} color="primary" disabled={!this.state.movieName.length}>
                                Поиск
                            </Button>
                        </Box>
                        <Box display="flex" alignItems="center">
                            <FormControl>
                                <InputLabel htmlFor="component-simple">Поиск имени актера</InputLabel>
                                <Input id="component-simple" value={this.state.actorName} onChange={this.nameActorOnChange.bind(this)} />
                            </FormControl>
                            <Button variant="contained" onClick={this.searchByNameOfActor.bind(this)} color="primary" disabled={!this.state.actorName.length}>
                                Поиск
                            </Button>
                        </Box>
                    </Box>
                    <Box display="flex" justifyContent="center" mt={2}>
                        <Button variant="contained" onClick={this.resetSort.bind(this)} color="primary">
                            Сбросить сортировку
                        </Button>
                    </Box>
                </Box>
                <Box display="flex" textAlign="center" mt={3} flexDirection="column" alignItems="center">
                    <h2>
                        Импорт фильмов
                    </h2>
                    <Box mt={2}>
                        <input type="file" name="filedata" onChange={this.onFileLoad.bind(this)}/>
                        <Button disabled={!this.state.file || this.state.isImport} variant="contained" onClick={this.importMovies.bind(this)} color="primary">
                            Совершить импорт
                        </Button>
                    </Box>
                    {   (this.state.isImport) ?
                            <CircularProgress color="secondary" />
                        : null
                    }
                </Box>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        movies: state.movies.movies
    }
}

export default connect(mapStateToProps)(SearchMovies)