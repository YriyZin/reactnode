import React, { Component} from 'react';
import MoviesItemDetail from '../presentation/MovieItemDetail';
import { connect } from 'react-redux'
import { fetchMoviesItem } from '../../actions/actions'
import { Box } from '@material-ui/core';

class MoviesArticle extends Component {


    async componentDidMount() {
        await this.props.dispatch(fetchMoviesItem(this.props.match.params.id));
    }

    render() {
        return (
            <Box display="flex" flexDirection="column" alignItems="center">
                <h2>Детальная информация</h2>
                <div>
                    { !this.props.moviesItemLoading ? <MoviesItemDetail data={this.props.moviesItem} /> : <div>Loading</div>}
                </div>
            </Box>
        )
    }
}

const mapStateToProps = state => {
    return {
        moviesItem: state.movies.moviesItem,
        moviesItemLoading: state.movies.moviesItemLoading
    }
}

export default connect(mapStateToProps)(MoviesArticle)