import constants from '../constants/actionTypes'

var initialState = {
  movies: [],
  moviesItem: {},
  moviesItemLoading: true
}

export default (state = initialState, action) => {

  var updated = Object.assign({}, state)

  switch(action.type) {

    case constants.MOVIE_RECEIVED:
        updated['movies'] = action.movies
        return updated;

    case constants.MOVIEITEM_RECEIVED:
        updated['moviesItem'] = action.moviesItem;
        updated['moviesItemLoading'] = false;
        return updated;
      
    case constants.MOVIEITEM_LOADING:
        updated['moviesItemLoading'] = true;
        return updated;
  
    default:
        return state
    }
}