import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import moviesReducer from '../reducers/moviesReducer';

const store = createStore(
  combineReducers({
    movies: moviesReducer
  }),
  applyMiddleware(
    thunk
  )
);

export default store;