import React, { Component } from 'react';
import './App.css';
import { Route, BrowserRouter } from 'react-router-dom';
import Home from './components/layouts/Home';
import Layout from './components/layouts/Layout';
import { Provider } from 'react-redux';
import store from './stores/store';
import MoviesArticle from './components/containers/MoviesArticle';
import AddMovie from './components/containers/AddMovie';
import 'typeface-roboto';

 
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <Layout>
            <Route exact path="/" component={Home}/>
            <Route path='/movies/:id' component={MoviesArticle}/>
            <Route path='/submit' component={AddMovie}/>
          </Layout>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;