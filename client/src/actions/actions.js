import actionTypes from '../constants/actionTypes';

function moviesItemReceived(moviesItem){
    return {
        type: actionTypes.MOVIEITEM_RECEIVED,
        moviesItem: moviesItem
    }
}

function moviesReceived(movies){
    return {
        type: actionTypes.MOVIE_RECEIVED,
        movies: movies
    }
}

export function availabilityCreateMovie(data){
    return dispatch => {
        return fetch('/movies/create/availability', { 
            method: 'POST', 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), 
            mode: 'cors'})
            .then( (response) => response.json() )
            .then( (data) => {return data} )
            .catch( (e) => console.log(e) );
    }    
}

export function submitAddMovies(data){
    return dispatch => {
        return fetch('/movies/create', { 
            method: 'POST', 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), 
            mode: 'cors'})
            .catch( (e) => console.log(e) );
    }    
}

export function searchMoviesByName(data) {
    return dispatch => {
        return fetch('/movies/', { 
            method: 'POST', 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), 
            mode: 'cors'})
            .then( (response) => response.json() )
            .then( (data) => dispatch(moviesReceived(data.data)) )
            .catch( (e) => console.log(e) );
    }    
}

export function searchByNameOfActor(data) {
    return dispatch => {
        return fetch('/movies/stars', { 
            method: 'POST', 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), 
            mode: 'cors'})
            .then( (response) => response.json() )
            .then( (data) => dispatch(moviesReceived(data.data)) )
            .catch( (e) => console.log(e) );
    }    
}

export function importMoviesFromFile(data) {
    return dispatch => {
        return fetch('/movies/import', { 
            method: 'POST',
            headers: {
                'enctype': 'multipart/form-data'
            },
            body: data, 
            mode: 'cors'})
            .then( (response) => response.json() )
            .then( (data) => {return data} )
            .catch( (e) => console.log(e) );
    }    
}

export function fetchMovies(){
    return dispatch => {
        return fetch(`/movies`)
        .then( (response) => response.json() )
        .then( (data) => dispatch(moviesReceived(data.data)) )
        .catch( (e) => console.log(e) );
    } 
}

export function removeMoviesItem(data){
    return dispatch => {
        return fetch('/movies/remove', { 
            method: 'DELETE', 
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), 
            mode: 'cors'})
            .catch( (e) => console.log(e) );
    }   
}

export function fetchMoviesItem(id){
    return dispatch => {
        return fetch(`/movies/${id}`)
        .then( (response) => response.json() )
        .then( (data) => dispatch(moviesItemReceived(data.data)))
        .catch( (e) => console.log(e) );
    } 
}