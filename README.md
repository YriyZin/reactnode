Client
To start the application on the local machine, you need to go along the / client path and run the npm install command, then you can launch the client application in development mode with the npm run start command and then deploy the default ar application to localhost:3000.

Server
To start the nodeJs server, go to the / server path and run the npm install and npm install -g nodemon command
then run the nodemon command, after which the server starts by default with localhost: 5000.

For the REST API, proxying has been done from localhost: 3000 to localhost: 5000. Redux was used to manage the state of the client application.
Redux is an evolution of Flux.
It follows three principles:
Single source of truth - The state for the entire application is in a single store.
State is read only - The only way to change state is to dispatch an action which will result in the state changing.
Changes are made with pure functions - These pure functions transform the state and are called reducers.

The library of ready-made components was used https://material-ui.com/
To verify props, the npm PropTypes package was used.
The npm react-router-dom package was used for navigation.

For the nodejs server, the Exrpess framework was used.

MongoDB was used as a database, or rather the service https://mlab.com/ for quick and easy start of working with the database.

For saving files, the Multer package was used.